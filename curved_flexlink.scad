/*
Théo Lisart  -- FabZero 2021 --

To build any one-point curvature beam we split the object into three part :

Generalized curved flexlink

----------------

License : MIT
*/


use <basic_structures/beam_curved.scad>
use <basic_structures/gage.scad>

$fn = 90;

// Model definition ---------------------------------------------------------------------------------------------------

hole_amount_left = 3;
hole_radius_left = 2.5;
gage_depth_left = 7;
gage_width_left = 16;
gage_height_left = 7.4;
orientation_left = "vert";   // Use this as default for this design

hole_amount_right = 3;
hole_radius_right = 2.5;
gage_depth_right = 7;
gage_width_right = 16;
gage_height_right = 7.4;
orientation_right = "vert";

beam_curve_radius = 55; //mm
beam_angle = 40;
length_first_beam = 10; //mm
length_second_beam = 24;
beam_thickness = 1;
beam_girth = 4;

distance_holes_right = 8;
distance_holes_left = 8;



// left gage
gage(hole_amount_left, hole_radius_left, gage_depth_left, gage_width_left ,gage_height_left, orientation_left,distance_holes_left);

// Beam
translate(v = [gage_width_left/2 + hole_radius_left, 0, 0])
translate(v = [length_first_beam/2, 0, 0])
beam(beam_angle, beam_curve_radius, length_first_beam, width, length_second_beam, beam_thickness, beam_girth);

// Translating along right gage length
translate(v = [(gage_width_right/2 + hole_radius_right)*cos(beam_angle), 0, (gage_width_right/2+ hole_radius_right)*sin(beam_angle)])

// translating along next beam
translate([(length_second_beam)*cos(beam_angle),0, (length_second_beam)*sin(beam_angle)])
// Translating along first beam and curve
translate([0,0, 2*beam_curve_radius*sin(beam_angle/2)*sin(beam_angle/2)])
translate([beam_curve_radius*sin(beam_angle),0, 0])

// Translating to first beam
translate(v = [length_first_beam, 0, 0])

// Translating at gage left position
translate(v = [gage_width_left/2 + hole_radius_left, 0, 0])
rotate(beam_angle, [0, -1, 0])
gage(hole_amount_right, hole_radius_right, gage_depth_right, gage_width_right ,gage_height_right, orientation_right, distance_holes_left);
