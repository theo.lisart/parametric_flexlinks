/*
Théo Lisart  -- FabZero 2021 --

An introduction to openScad : building flexlinks set (or how to steal simple design by eyeballing it).
Simple model : Straight flexylinks

            ---
           | 0 |
           | 0 |
             |
  -----      |        ------
| 0  0 |-----0------| 0  0 |
  -----      |        ------
             |
           | 0 |
           | 0 |
            ---

Modular idea : using rotation to build more complex structures
Rotation angle around the center

License : MIT
*/

// Includes

use <basic_structures/flex_module.scad>;
use <basic_structures/gage.scad>;

$fn = 120;

// Parameters definition

amount_flex = 2;                                                         ;
base_angle = 40;

// One side structure
hole_amount_left = 2;
hole_radius_left = 1;
gage_depth_left = 3;
gage_width_left = 4;
gage_height_left = 1;
orientation_left = "vert";   // Other option: Horizontal, further rotations see gage.scad

// Other side structure
hole_amount_right = 4;
hole_radius_right = 1;
gage_depth_right = 3;
gage_width_right = 9;
gage_height_right = 1;
orientation_right = "vert";

// Beams
beam_curve = 0.5; //mm
beam_length = 20; //mm
beam_thickness = 0.4;
beam_girth = 0.6;

// Center hole
hole_radius = 1;
gage_depth = 3;
gage_width = 0.1;
gage_height = 1;
orientation = "vert";

// Complexity : getting rid of the excess elements
hole_amount = 0;

difference(){
    for(i = [1 : amount_flex]){

        // Centering the object on the reference point
        half_length = gage_width_left/2 + beam_length/2 + hole_radius_left;
        rotate(base_angle*i, [0, 1, 0])
        translate(v = [-half_length, 0, 0])
        basic_flex(hole_amount_left, hole_radius_left, gage_depth_left, gage_width_left, gage_height_left, orientation_left, hole_amount_right, hole_radius_right, gage_depth_right, gage_width_right,gage_height_right, orientation_right, beam_length, beam_thickness, beam_girth);
        }

    // Adding a support structure at the center - r
    hole_amount = 0;
    gage(hole_amount, hole_radius, gage_depth, gage_width ,gage_height, orientation);
 }


// Restauring center
hole_amount = 1;   // Redifining the center hole
gage(hole_amount, hole_radius, gage_depth, gage_width ,gage_height, orientation);

//
