# parametric_flexlinks

Théo Lisart  -- FabZero 2021

An introduction to openScad : building flexlinks set (or how to steal simple design by eyeballing it).
Simple model : goal is to iteratively build from the simpliest componants to the more complicated structure.

```
____  __    ____  _  _  __    ____  _  _  _  _  ___
( ___)(  )  ( ___)( \/ )(  )  (_  _)( \( )( )/ )/ __)
)__)  )(__  )__)  )  (  )(__  _)(_  )  (  )  ( \__ \
(__)  (____)(____)(_/\_)(____)(____)(_)\_)(_)\_)(___/
```

In hope of learning Openscad and LibreCAD, we build parametric drawings of the flexlink project from BYU-CMR.

- basic_structures   // modules folder
  |
  --> - beam.scad
      - flex_module.scad
      - gage.scad

- flex.scad
- symmetric_tension.scad    --> Symmetries around the top of the part
- tension_structure.scad    --> Helicoidal shape
- simple_z_spring.scad      --> Flexlink spring (one diagonal beam)
- double_z_spring.scad      --> Flexlink spring (two diagonal beam)
Under MIT license. Feel free to share and do whatever you want with those files.
