/*
Théo Lisart  -- FabZero 2021 --

Complient design : Z simple Z spring
----------------

License : MIT
*/


// Includes

use <basic_structures/beam.scad>;
use <basic_structures/gage.scad>;
$fn = 90;
//


// Top gage
hole_amount_left = 3;
hole_radius_left = 2.5;
gage_depth_left = 7;
gage_width_left = 16;
gage_height_left = 4.4;      // Parameter to change for the whole thing
orientation_left = "vert";   // Use vert as default for this design 

distance_holes_left = 8;

// Bottom gage
hole_amount_right = 3;
hole_radius_right = 2.5;
gage_depth_right = 7;
gage_width_right = 16;
gage_height_right = 4.4;
orientation_right = "vert";

distance_holes_right = 8;


// Model definition 
spring_resting_distance = 10;
ang = 37;  // Degrees
tolerance_offset = 0.2;

// Beam 
/*One could use the curved beam to find interesting shapes, it is not done here as a test and getting to a known model, needs improving*/

beam_length_1 = (gage_width_left + 2*hole_radius_left)/cos(ang);   // Projected along the first gage
beam_thickness_1 = gage_height_left/2 - tolerance_offset;
beam_girth_1 = 0.4;

beam_length_2 = (gage_width_left + 2*hole_radius_left)/cos(ang);   // Projected along the first gage
beam_thickness_2 = gage_height_left;
beam_girth_2 = 0.4;

module sprint_double(ang, hole_amount_left, hole_radius_left, gage_depth_left, gage_width_left, gage_height_left, orientation_left, distance_holes_left, hole_amount_right, hole_radius_right, gage_depth_right, gage_width_right, gage_height_right, orientation_right, distance_holes_right, beam_length_1, beam_thickness_1, beam_girth_1){
    union(){
        
        
        // Second gage 
                // First gage
        translate([0, 0, -(beam_length_1 + 2*hole_radius_right)*sin(ang)])
        gage(hole_amount_right, hole_radius_right, gage_depth_right, gage_width_right, gage_height_right, orientation_right, distance_holes_right);



        // Beam 2
        translate([0,(gage_height_left/2 - beam_thickness_1/2 ), -((beam_length_1/2 + hole_radius_left)*sin(ang))])
        rotate(ang, [0, -1, 0])
        cube([beam_length_1, beam_thickness_1, beam_girth_1], center=true);
        
        
        // Beam 1
        translate([0,- (gage_height_left/2 - beam_thickness_1/2 ), -((beam_length_1/2 + hole_radius_left)*sin(ang))])
        rotate(ang, [0, 1, 0])
        cube([beam_length_1, beam_thickness_1, beam_girth_1], center=true);
       
        // First gage
        gage(hole_amount_left, hole_radius_left, gage_depth_left, gage_width_left, gage_height_left, orientation_left, distance_holes_left);
        
    }
}


sprint_double(ang, hole_amount_left, hole_radius_left, gage_depth_left, gage_width_left, gage_height_left, orientation_left, distance_holes_left, hole_amount_right, hole_radius_right, gage_depth_right, gage_width_right, gage_height_right, orientation_right, distance_holes_right, beam_length_1, beam_thickness_1, beam_girth_1);