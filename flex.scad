/*
Théo Lisart  -- FabZero 2021 --

An introduction to openScad : building flexlinks set (or how to steal simple design by eyeballing it).
Simple model : Straight flexylinks


  -----              ------
| 0  0 |------------| 0  0 |
  -----              ------

Modular idea : gage 1 + beam

License : MIT
*/
// Includes

use <basic_structures/beam.scad>;
use <basic_structures/gage.scad>;
$fn = 90;
//

// Model definition ---------------------------------------------------------------------------------------------------

hole_amount_left = 2;
hole_radius_left = 1;
gage_depth_left = 3;
gage_width_left = 10;
gage_height_left = 1;
orientation_left = "horiz";   // Other option: Horizontal, further rotations see gage.scad

hole_amount_right = 5;
hole_radius_right = 1;
gage_depth_right = 3;
gage_width_right = 15;
gage_height_right = 1;
orientation_right = "vert";

beam_curve = 0.5; //mm
beam_length = 10; //mm
beam_thickness = 0.4;
beam_girth = 0.6;


/*
    Left gage : amount holes + geometry
    Right gage : amount holes + geometry
    middle beam : length + connection
*/


// Drawing ------------------------------------------------------------------------------------------------------------

// left gage
gage(hole_amount_left, hole_radius_left, gage_depth_left, gage_width_left ,gage_height_left, orientation_left);

// Middle beam
translate(v = [beam_length/2 + gage_width_left/2 + hole_radius_left, 0, 0])
beam(beam_length, 0, beam_thickness, beam_girth);

// right gage
translate(v = [beam_length + gage_width_left/2 + gage_width_right/2 + hole_radius_right*2, 0, 0])
gage(hole_amount_right, hole_radius_right, gage_depth_right, gage_width_right ,gage_height_right, orientation_right);
