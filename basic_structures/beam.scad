/*
Théo Lisart  -- FabZero 2021 --

An introduction to openScad : building flexlinks set (or how to steal simple design by eyeballing it).
Simple model : Straight flexylinks


----------------
License : MIT
*/

module beam(radius, ang, width, height){
    if(ang > 0){
        rotate_extrude(angle = ang) translate([radius, 0, 0]) square(size = [height, width], center = true);
    }
    else if(ang ==0){
        cube([radius, width,height], center=true);
    }
}
