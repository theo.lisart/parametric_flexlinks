/*
Théo Lisart  -- FabZero 2021 --

An introduction to openScad : building flexlinks set (or how to steal simple design by eyeballing it).
Simple model : Straight flexylinks


  -----              ------
| 0  0 |            | 0  0 |
  -----              ------

License : MIT
*/


module gage(hole_amount, hole_radius, depth, width, height, orientation, offset_init){

    /*
        Gage module defining the geometries of the flexgage
        Note : this could be obviously written without code redundancy, and should be written
               without redoundancy.
    */

    // Drawing --------------------------------------------------------------------------------------------------------

    height_cyl = height;
    radius_cyl = depth/2;

    total_length = 2*radius_cyl + width;
    offset = 0;

    offset = offset_init;

    /*
    if(offset_init > 0){
        offset = offset_init;
    }else{offset = total_length/hole_amount;}
    */
    epsilon = 0.1;

    // Taking the union of the cube and a cylinder at both extremities of the cube


    if(hole_amount == 0 && orientation == "vert"){
        rotate(90, [1, 0, 0])
        union(){
        cube([width,depth,height], center=true);
        translate(v = [width/2, 0, 0])
        cylinder(h = height_cyl, r = radius_cyl, center = true);
        translate(v = [-width/2, 0, 0])
        cylinder(h = height_cyl, r = radius_cyl, center = true);
        }
    }

    else if(hole_amount == 0 && orientation == "horiz"){
        union(){
        cube([width,depth,height], center=true);
        translate(v = [width/2, 0, 0])
        cylinder(h = height_cyl, r = radius_cyl, center = true);
        translate(v = [-width/2, 0, 0])
        cylinder(h = height_cyl, r = radius_cyl, center = true);
        }
    }
    else if(orientation == "vert"){
    rotate(90, [1, 0, 0])

        difference(){
            // basis
            union(){
                cube([width,depth,height], center=true);
                translate(v = [width/2, 0, 0])
                cylinder(h = height_cyl, r = radius_cyl, center = true);
                translate(v = [-width/2, 0, 0])
                cylinder(h = height_cyl, r = radius_cyl, center = true);
            }
            // Boring holes
            for (i = [0 : hole_amount]){
                translate(v = [width/2 - i*offset, 0, 0])
                cylinder(h = height_cyl + epsilon, r = hole_radius, center = true);
            }
        }
    }

    else if(orientation == "horiz"){
        difference(){
            // basis
            union(){
                cube([width,depth,height], center=true);
                translate(v = [width/2, 0, 0])
                cylinder(h = height_cyl, r = radius_cyl, center = true);
                translate(v = [-width/2, 0, 0])
                cylinder(h = height_cyl, r = radius_cyl, center = true);
            }
            // Boring holes
            for (i = [0 : hole_amount]){
                translate(v = [width/2 - i*offset, 0, 0])
                cylinder(h = height_cyl + epsilon, r = hole_radius, center = true);
            }
        }
    }
}
