/*
Théo Lisart  -- FabZero 2021 --

An introduction to openScad : building flexlinks set (or how to steal simple design by eyeballing it).
Simple model : Straight flexylinks


  -----              ------
| 0  0 |------------| 0  0 |
  -----              ------

Modular idea : gage 1 + beam

License : MIT
*/
// Includes
use <beam.scad>;
use <gage.scad>;


module basic_flex(hole_amount_left, hole_radius_left, gage_depth_left, gage_width_left, gage_height_left, orientation_left, hole_amount_right, hole_radius_right,gage_depth_right, gage_width_right,gage_height_right, orientation_right, beam_length, beam_thickness, beam_girth){

    /*
        Left gage : amount holes + geometry
        Right gage : amount holes + geometry
        middle beam : length + connection
    */


    // Drawing ------------------------------------------------------------------------------------------------------------

    // left gage
    gage(hole_amount_left, hole_radius_left, gage_depth_left, gage_width_left ,gage_height_left, orientation_left);

    // Middle beam
    translate(v = [beam_length/2 + gage_width_left/2 + hole_radius_left, 0, 0])
    beam(beam_length, 0, beam_thickness, beam_girth);

    // right gage
    translate(v = [beam_length + gage_width_left/2 + gage_width_right/2 + hole_radius_right*2, 0, 0])
    gage(hole_amount_right, hole_radius_right, gage_depth_right, gage_width_right ,gage_height_right, orientation_right);
}
