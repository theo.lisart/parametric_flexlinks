/*
Théo Lisart  -- FabZero 2021 --

To build any one-point curvature beam we split the object into three part :

straight beam - cruved beam - straight beam

----------------

License : MIT
*/

// Dimensions
width = 1;
height = 2;

// First beam
length_first_beam = 25;

// Curved beam
radius = 20;     // Curvature radius
ang = 100;
center = true;
convexity = 30;

// second beam
length_second_beam = 100;

module beam(ang, radius, length_first_beam, width, length_second_beam, height, width){
    if(ang > 0){

        // placing the beam back into easiest to hand position
        rotate(ang, [0, -1, 0])

        union(){

                // Second beam - plaing it at the end of the curve
                translate([0,0, - length_first_beam/2*sin(ang) - 2*radius*sin(ang/2)*sin(ang/2)])
                translate([length_first_beam/2*cos(ang) + length_second_beam/2 + radius*sin(ang),0, 0])
                cube(size = [length_second_beam, width, height], center=true);
                rotate(ang, [0, 1, 0])

                // Both elements
                union(){

                    // Curved part
                    translate([length_first_beam/2, 0, radius])
                    rotate(90, [0, 1, 0])
                    rotate(90, [1, 0, 0])
                    rotate_extrude(angle =ang, convexity =convexity)
                    translate([radius, 0])
                    square(size = [height, width], center = true);

                    // First beam
                    cube(size = [length_first_beam, width, height], center=true);
                }
            }
        }
    else if(ang ==0){
        cube([radius, width,height], center=true);
    }
}



beam(ang, radius, length_first_beam, width, length_second_beam, height, width);
